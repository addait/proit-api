const express = require("express");
const router = express.Router();
const Type = require("../controllers/Type");

router.post("/type/datatype", Type.DataType);
router.post("/type/adddatatype", Type.AddDataType);
router.post("/type/datatypeedit", Type.DataTypeEdit);
router.post("/type/editdatatype", Type.EditDataType);
router.post("/type/deletedatatype", Type.DeleteDataType);

module.exports = router;
