const express = require("express");
const router = express.Router();
const Supplies = require("../controllers/Supplies");

router.post("/supplies/groupsupllies", Supplies.GroupSupplies);
router.post("/supplies/addgroupsupplies", Supplies.AddGroupSupplies);
router.post("/supplies/dataeditgroupsupplies", Supplies.DataEditGroupSupplies);
router.post("/supplies/editgroupsupplies", Supplies.EditGroupSupplies);
router.post("/supplies/deletegroupsupplies", Supplies.DeleteGroupSupplies);

module.exports = router;
