const express = require("express");
const router = express.Router();
const Camera = require("../controllers/Camera");

router.post("/camera/cctv", Camera.CCTV);
router.post("/camera/gencctv", Camera.gen_addphoto);
router.post("/camera/cctvinsert", Camera.InsertCCTV);
router.post("/camera/cctvupdate", Camera.UpdateCCTV);
router.post("/camera/cctvdelete", Camera.DeleteCCTV);
router.post("/camera/cctvimg", Camera.UpdateCCTV_IMAGE);
router.post("/camera/cctvimgdelete", Camera.DeleteCCTV_IMAGE);

router.post("/camera/camera", Camera.camera);
router.post("/camera/gencamera", Camera.gen_addphotocam);
router.post("/camera/camerainsert", Camera.InsertCamera);
router.post("/camera/cameraupdate", Camera.UpdateCamera);
router.post("/camera/cameradelete", Camera.DeleteCamera);
router.post("/camera/cameraimg", Camera.UpdateCamera_IMAGE);
router.post("/camera/cameraimgdelete", Camera.DeleteCamera_IMAGE);



module.exports = router;
