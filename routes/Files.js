const express = require("express");
const router = express.Router();
const Files = require("../controllers/Files");

router.get("/files", Files.files);
router.get("/files/check/:fileid", Files.checkFile);
router.post("/files/add", Files.addFile);
router.post("/files/edit", Files.editFile);
router.get("/files/delete/:fileid", Files.deleteFile);

module.exports = router;
