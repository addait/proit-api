const express = require("express");
const router = express.Router();
const Data = require("../controllers/Data");

router.post("/database", Data.Database);
router.post("/detailsdatabase", Data.DetailsDatabase);
router.post("/detailstable", Data.DetailsTable);
router.post("/loadtable", Data.LoadTable);

router.post("/basename", Data.Basename);
router.post("/addbasename", Data.AddBasename);

router.post("/tableshow", Data.TableShow);

router.post("/adddatadic", Data.AddDataDic);

router.post("/loaddataedit", Data.LoadDataEdit);
router.post("/editdatadic", Data.EditDataDic);
router.post("/deletedatadic", Data.DeleteDataDic);

router.post("/loadbasename", Data.LoadBasename);
router.post("/loaddatadictionaryshow", Data.LoadDataDictionaryShow);

module.exports = router;
