const express = require("express");
const router = express.Router();
const Users = require("../controllers/Users");

router.get("/users", Users.users);
router.get("/users/check/:id", Users.checkUsers);
router.get("/users/permission/:id", Users.permission);
router.get("/users/permission/files/:id", Users.permissionFiles);
router.post("/users/add", Users.addUsers);
router.post("/users/edit", Users.editUsers);
router.get("/users/delete/:id", Users.deleteUsers);

module.exports = router;
