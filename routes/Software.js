const express = require("express");
const router = express.Router();
const Software = require("../controllers/Software");

router.get("/software/datasoftware", Software.DataSoftware);
router.post("/software/dataasset", Software.DataAsset);
router.post("/software/datatype", Software.DataType);
router.post("/software/databrand", Software.DataBrand);
router.post("/software/dataseller", Software.DataSeller);
router.post("/software/datalocation", Software.DataLocation);
router.post("/software/genidasset", Software.GenIDasset);
router.post("/software/addasset", Software.AddAsset);
router.post("/software/datasoftwareedit", Software.DataSoftwareEdit);
router.post("/software/editasset", Software.EditAsset);

router.post("/software/deleteasset" , Software.DeleteAsset);

module.exports = router;
