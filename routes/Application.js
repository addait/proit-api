const express = require("express");
const router = express.Router();
const Application = require("../controllers/Application");

router.post("/application/dataapp", Application.DataApp);
router.post("/application/genappid", Application.GenAppID);
router.post("/application/addapp", Application.AddApp);
router.post("/application/dataappedit", Application.DataAppEdit);
router.post("/application/editapp", Application.EditApp);
router.post("/application/deleteapp", Application.DeleteApp);

module.exports = router;
