const express = require("express");
const router = express.Router();
const API = require("../controllers/API_upload");

router.post("/api", API.APIUPLOAD);


module.exports = router;
