const express = require("express");
const router = express.Router();
const Computer = require("../controllers/Computer");

router.post("/computer/userhrm", Computer.UserHRM);
router.post("/computer/depthrm", Computer.DeptHRM);
router.get("/computer/datacomputer", Computer.DataComputer);
router.post("/computer/os", Computer.OS);
router.post("/computer/software", Computer.Software);
router.post("/computer/softwareadda", Computer.SoftwareAdda);
router.post("/computer/addmain", Computer.AddMain);
router.post("/computer/loaddataedit", Computer.LoadDataEdit);
router.post("/computer/loadsoftwareedit", Computer.LoadSoftwareEdit);
router.post("/computer/loadsoftwareaddaedit", Computer.LoadSoftwareAddaEdit);
router.post("/computer/editmain", Computer.EditMain);
router.post("/computer/deletedata", Computer.DeleteData);

module.exports = router;
