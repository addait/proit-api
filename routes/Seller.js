const express = require("express");
const router = express.Router();
const Supplier = require("../controllers/Seller");

router.post("/supplier/datasupplier", Supplier.DataSupplier);

module.exports = router;
