const express = require("express");
const router = express.Router();
const Brand = require("../controllers/Brand");

router.post("/brand/databrand", Brand.DataBrand);
router.post("/brand/genidbrand", Brand.GenIDBrand);
router.post("/brand/adddatabrand", Brand.AddDataBrand);
router.post("/brand/databrandedit", Brand.DataBrandEdit);
router.post("/brand/editdatabrand", Brand.EditDataBrand);
router.post("/brand/deletedatabrand", Brand.DeleteDataBrand);

module.exports = router;
