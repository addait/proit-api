const express = require("express");
const router = express.Router();
const SuppliesMRP = require("../controllers/SuppliesMRP");

router.post("/suppliesmrp/datasuppliesmrp", SuppliesMRP.DataSuppliesMRP);
router.post("/suppliesmrp/datasparepartmrp", SuppliesMRP.DataSparepartMRP);

module.exports = router;
