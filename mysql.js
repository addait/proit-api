const mysql = require("mysql");

const dbConfigMySQL = {
  host: "10.32.0.85",
  user: "root",
  password: "@$%Adda#Sql2017",
  database: "COMPUTER"
};

const executeSql = query =>
  new Promise(async (resolve, reject) => {
    const pool = mysql.createPool(dbConfigMySQL);
    pool.query(query, (error, results) => {
      if (error) {
        resolve({ error: true, data: error });
      } else {
        resolve({ error: false, data: results });
      }
    });
  });

function queryPromise(query, connection) {
  return new Promise((resolve, reject) => {
    connection.query(query, err => {
      if (err) {
        return connection.rollback(() => {
          reject(err);
        });
      }
      resolve(true);
    });
  });
}

const executeTransaction = async (queries, callback) => {
  const pool = mysql.createPool(dbConfigMySQL);
  pool.getConnection((err, connection) => {
    if (err) throw callback(err);
    connection.beginTransaction(async err => {
      if (err) throw callback(err);
      for (let i = 0; i < queries.length; i++) {
        try {
          await queryPromise(queries[i], connection);
        } catch (err) {
          return callback(err);
        }
      }
      connection.commit(err => {
        if (err) {
          return connection.rollback(() => {
            callback(err);
          });
        }
        callback(false);
      });
    });
  });
};

module.exports.executeSql = executeSql;
module.exports.executeTransaction = executeTransaction;