const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.DataBrand = async function(req, res) {
  let strsql = `SELECT a.brandID , a.brandName , COUNT(a.brandID) as quantity , 
  a.preby , a.predate , a.lastby , a.lastdate 
  FROM [DBproit].[dbo].[brand] a
  LEFT JOIN [DBproit].[dbo].[asset] b
  ON a.brandID = b.brandID
  GROUP BY a.brandID , a.brandName , 
  a.preby , a.predate , a.lastby , a.lastdate`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.GenIDBrand = async (req, res) => {
  let strsql = `SELECT MAX(brandID) as maxbrand FROM [DBproit].[dbo].[brand]`;
  let result = await resql.sql_query(strsql);
  let a;
  if (result.recordset[0].maxbrand === null) {
    a = "001";
  } else {
    a = numeral(parseInt(result.recordset[0].maxbrand) + 1).format("000");
  }
  res.send({ status: true, data: a });
};

exports.AddDataBrand = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let strsql = `INSERT INTO [DBproit].[dbo].[brand]
  (brandID , brandName , preby , predate) VALUES 
  ('${req.body.brandID}' , '${req.body.brandName}' ,
   '${req.body.preby}' , '${current}')`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};

exports.DataBrandEdit = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[brand] 
    WHERE brandID = '${req.body.brandID}'`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditDataBrand = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let sqlArray = [];
  let strsqldelete = `DELETE FROM [DBproit].[dbo].[brand] 
  WHERE brandID = '${req.body.brandID}'`;
  sqlArray.push({ sql: strsqldelete });

  let strsql = `INSERT INTO [DBproit].[dbo].[brand]
      (brandID , brandName , preby , predate , lastby , lastdate) VALUES 
      ('${req.body.brandID}' , '${req.body.brandName}' ,
       '${req.body.preby}' , '${req.body.predate}' , 
       '${req.body.lastby}' , '${current}')`;
  sqlArray.push({ sql: strsql });

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};

exports.DeleteDataBrand = async (req, res) => {
  let strsql = `DELETE FROM [DBproit].[dbo].[brand] WHERE brandID = '${req.body.brandID}'`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};
