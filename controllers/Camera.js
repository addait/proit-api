const resql = require("../sql");
const multer = require("multer");
const moment = require("moment");
const fs = require("fs");
const path = "C:/AppServ/www/PHOTO/CCTV";
const path_cam = "C:/AppServ/www/PHOTO/Camera";

exports.CCTV = async (req, res) => {
  let strsql = `SELECT a.*
    FROM [DBcomputer].[dbo].[cctv] a
    where a.cctvID like '${req.body.cctv}'
    ;`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.camera = async (req, res) => {
  let strsql = `SELECT a.*
    FROM [DBcomputer].[dbo].[camera] a
    where a.cameraID like '${req.body.camera}'
    ;`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.gen_addphoto = async (req, res) => {
  let h = "CAM";
  let x;
  let maxid = `SELECT max(a.picname) as maxID
  FROM [DBcomputer].[dbo].[cctv] a
  where a.picname like '${h}%'
  ;`;
  let id = await resql.sql_query(maxid);
  if (id.recordset[0].maxID === null) {
    x = "00001";
  } else {
    x = "00000" + (parseInt(id.recordset[0].maxID.substring(3, 8)) + 1);
    x = x.substring(x.length - 5, x.length);
  }
  h = h + x;
  res.send({ status: true, data: h });
};

exports.DeleteCCTV_IMAGE = async (req, res) => {
  console.log(req.body.id)
  let strsql = `
    SELECT a.*
    FROM [DBcomputer].[dbo].[cctv] a
    where a.cctvID like '%${req.body.id}%'
    ;`;
  let result = await resql.sql_query(strsql);
  if (result.recordset.length >= 1) {
    if(result.recordset[0].picname === '' || result.recordset[0].picname === null){
      return res.send({ success: true, status: false });
    }else{
      let filePath = path + "/" + result.recordset[0].picname + ".jpg";
      if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath);
        return res.send({ success: true, status: true });

      }
    }
  }
};

exports.UpdateCCTV_IMAGE = async (req, res) => {
  let namedata = "";

  let storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, path);
    },
    filename: function(req, file, cb) {
      namedata = file.originalname;
      cb(null, file.originalname + ".jpg");
    }
  });

  let upload = multer({ storage: storage }).single("file");

  upload(req, res, async err => {
    if (err) {
      return res.send({ success: true, status: false, name: namedata });
    } else {
      return res.send({ success: true, status: true, name: namedata });
    }
  });
};

exports.UpdateCCTV = async (req, res) => {
  let strsql = `UPDATE [DBcomputer].[dbo].[cctv]
    SET [status] = '${req.body.status}', 
    [model] = '${req.body.model}', 
    [serialNo] = '${req.body.serialNo}', 
    [firmware] = '${req.body.firmware}', 
    [ip] = '${req.body.ip}', 
    [startdate] = '${req.body.startdate}', 
    [waranty] = '${req.body.waranty}', 
    [remark] = '${req.body.remark == null ? "" : req.body.remark}',
    [picname] = '${req.body.picname === null || req.body.picname === "" ?  "" : req.body.picname}', 
    [lastby] = '${req.body.lastby}',
    [lastdate] = '${moment().format("YYYY-MM-DD HH:mm:ss")}'
    WHERE [cctvID] = '${req.body.cctvID}'
    `;
    console.log(strsql)
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DeleteCCTV = async (req, res) => {
  let strsql = `DELETE FROM [DBcomputer].[dbo].[cctv] where [cctvID] = '${req.body.id}' `;
  // console.log(strsql)
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.InsertCCTV = async (req, res) => {
  let h = "BOX";
  let x;
  let maxid = `select max(cctvID) as maxID FROM [DBcomputer].[dbo].[cctv] `;
  let id = await resql.sql_query(maxid);
  if (id.recordset[0].maxID === null) {
    x = "00001";
  } else {
    x = "00000" + (parseInt(id.recordset[0].maxID.substring(3, 8)) + 1);
    x = x.substring(x.length - 5, x.length);
  }
  h = h + x;
  let strsql = `INSERT INTO [DBcomputer].[dbo].[cctv] ([cctvID]
        ,[status]
        ,[model]
        ,[serialNo]
        ,[firmware]
        ,[ip]
        ,[name]
        ,[startdate]
        ,[waranty]
        ,[remark]
        ,[picname]
        ,[preby]
        ,[predate]) 
        values
        (
          '${h}',
          '${req.body.status}',
          '${req.body.model}',
          '${req.body.serialNo}',
          '${req.body.firmware}',
          '${req.body.ip}',
          '${req.body.name}',
          '${req.body.startdate}',
          '${req.body.waranty}',
          '${req.body.remark}',
          '${
            req.body.picname === null || req.body.picname === ""
              ? ""
              : req.body.picname
          }',
          '${req.body.preby}', 
          '${moment().format("YYYY-MM-DD HH:mm:ss")}'          
        )`;
  // console.log(strsql);
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.UpdateCamera = async (req, res) => {
  let strsql = `UPDATE [DBcomputer].[dbo].[camera] set [cctvID] = '${req.body.cctvID}',
                       [status] = '${req.body.status}',
                       [asCode]= '${req.body.asCode}',
                       [model]= '${req.body.model}',
                       [serialNo]= '${req.body.serialNo}',
                       [firmware]= '${req.body.firmware}',
                       [type]= '${req.body.type}',
                       [ip]= '${req.body.ip}',
                       [location]= '${req.body.location}',
                       [startWaranty]= '${req.body.startWaranty}',
                       [waranty]= '${req.body.waranty}',
                       [picname]= '${req.body.picname === null || req.body.picname === "" ?  "" : req.body.picname}',
                       [remark]= '${req.body.remark == null ? "" : req.body.remark}',
                       [notfity]= '${req.body.notfity ? 1 : 0}',
                       [startdate]= '${req.body.startdate}',
                       [price]     = '${req.body.price}',
                       [lastby] = '${req.body.lastby}',
                       [lastdate] = '${moment().format("YYYY-MM-DD HH:mm:ss")}'
WHERE [cameraID] = '${req.body.cameraID}'
    `;
  // console.log(strsql)
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DeleteCamera= async (req, res) => {
  let strsql = `DELETE FROM [DBcomputer].[dbo].[camera] where [cameraID] = '${req.body.id}' `;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.InsertCamera= async (req, res) => {
  let h = "CAM";
  let x;
  let maxid = `select max(cameraID) as maxID FROM [DBcomputer].[dbo].[camera] `;
  let id = await resql.sql_query(maxid);
  if (id.recordset[0].maxID === null) {
    x = "00001";
  } else {
    x = "00000" + (parseInt(id.recordset[0].maxID.substring(3, 8)) + 1);
    x = x.substring(x.length - 5, x.length);
  }
  h = h + x;
  let strsql = `INSERT INTO [DBcomputer].[dbo].[camera] (
    [cameraID]
    ,[cctvID]
    ,[status]
    ,[asCode]
    ,[model]
    ,[serialNo]
    ,[firmware]
    ,[type]
    ,[ip]
    ,[location]
    ,[startWaranty]
    ,[waranty]
    ,[picname]
    ,[remark]
    ,[notfity]
    ,[startdate]
    ,[price]
    ,[preby]
    ,[predate]) 
        values
        (
          '${h}',
          '${req.body.cctvID}',
          '${req.body.status}',
          '${req.body.asCode}',
          '${req.body.model}',
          '${req.body.serialNo}',
          '${req.body.firmware}',
          '${req.body.type}',
          '${req.body.ip}',
          '${req.body.location}',
          '${req.body.startWaranty}',
          '${req.body.waranty}',
          '${
            req.body.picname === null || req.body.picname === ""
              ? ""
              : req.body.picname
          }',
          '${req.body.remark}',
          '${req.body.notfity ? 1 : 0}',
          '${req.body.startdate}',
          '${req.body.price}',    
          '${req.body.preby}', 
          '${moment().format("YYYY-MM-DD HH:mm:ss")}'          
        )`;
  // console.log(strsql);
  // console.log(req.body.picname)
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};


exports.gen_addphotocam = async (req, res) => {
  let h = "CAM";
  let x;
  let maxid = `SELECT max(a.picname) as maxID
  FROM [DBcomputer].[dbo].[camera] a
  where a.picname like '${h}%'
  ;`;
  let id = await resql.sql_query(maxid);
  if (id.recordset[0].maxID === null) {
    x = "00001";
  } else {
    x = "00000" + (parseInt(id.recordset[0].maxID.substring(3, 8)) + 1);
    x = x.substring(x.length - 5, x.length);
  }
  h = h + x;
  res.send({ status: true, data: h });
};

exports.DeleteCamera_IMAGE = async (req, res) => {
  // console.log(req.body.id)
  let strsql = `
    SELECT a.*
    FROM [DBcomputer].[dbo].[camera] a
    where a.[cameraID] like '%${req.body.id}%'
    ;`;
  let result = await resql.sql_query(strsql);
  // console.log(result.recordset)
  if (result.recordset.length >= 1) {
    if(result.recordset[0].picname === '' || result.recordset[0].picname === null){
      return res.send({ success: true, status: false });
    }else{
      let filePath = path_cam + "/" + result.recordset[0].picname;
      if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath);
        return res.send({ success: true, status: true });

      }
    }
  }
};

exports.UpdateCamera_IMAGE = async (req, res) => {
  let namedata = "";

  let storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, path_cam);
    },
    filename: function(req, file, cb) {
      
      namedata = file.originalname;
      cb(null, file.originalname + ".jpg");
    }
  });

  let upload = multer({ storage: storage }).single("file");

  upload(req, res, async err => {
    if (err) {
      return res.send({ success: true, status: false, name: namedata });
    } else {
      return res.send({ success: true, status: true, name: namedata });
    }
  });
};