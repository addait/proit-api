const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.GroupSupplies = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[itemtype]`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.AddGroupSupplies = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let strsql = `INSERT INTO [DBproit].[dbo].[itemtype] 
  (typeID , typeName , web , preby , predate)
  VALUES ('${req.body.typeID}' , '${req.body.typeName}' , '0' ,
  '${req.body.preby}' , '${current}')`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};

exports.DataEditGroupSupplies = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[itemtype] WHERE typeID = '${req.body.typeID}'`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditGroupSupplies = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let strsql = `UPDATE [DBproit].[dbo].[itemtype] SET
    typeName = '${req.body.typeName}' , lastby = '${req.body.lastby}' ,
    lastdate = '${current}' WHERE typeID = '${req.body.typeID}' `;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};

exports.DeleteGroupSupplies = async (req, res) => {
  let strsql = `DELETE FROM [DBproit].[dbo].[itemtype] WHERE typeID = '${req.body.typeID}'`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};
