const resql = require("../sql");
const sql = require("mssql");
const moment = require("moment");
const numeral = require("numeral");

exports.Database = async function(req, res) {
  const strsql = `SELECT name FROM master.dbo.sysdatabases
    WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb') ORDER BY name asc`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DetailsDatabase = async function(req, res) {
  const strsql = `SELECT * FROM ${req.body.database}.INFORMATION_SCHEMA.TABLES
  WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY TABLE_NAME asc`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DetailsTable = async function(req, res) {
  const strsql = `SELECT * FROM ${req.body.database}.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '${req.body.table}'`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.LoadTable = async function(req, res) {
  const strsql = `SELECT TABLE_NAME FROM ${req.body.database}.INFORMATION_SCHEMA.TABLES 
  WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY TABLE_NAME asc`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.Basename = async function(req, res) {
  const strsql = `SELECT * FROM [DBcomputer].[dbo].[basename]`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.AddBasename = async function(req, res) {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  const doc = `SELECT max(dataid) as idmax FROM [DBcomputer].[dbo].[basename]`;
  const resdoc = await resql.sql_query(doc);
  let d;
  let sv = "BS";
  if (resdoc.recordset[0].idmax === null) {
    d = "001";
  } else {
    d = parseInt(resdoc.recordset[0].idmax.substr(2, 3)) + 1;
  }
  sv = sv + numeral(d).format("000");
  const strsqlchk = `SELECT * FROM [DBcomputer].[dbo].[basename] WHERE dbname = '${req.body.basename}'`;
  const resultchk = await resql.sql_query(strsqlchk);
  if (resultchk.recordset.length === 0) {
    const strsql = `INSERT INTO [DBcomputer].[dbo].[basename] 
  (dataid , dbname , ipserver , preby , predate) VALUES 
  ('${sv}' , '${req.body.basename}' , '${req.body.server}' , '${req.body.preby}' , '${current}')
  `;
    if (await resql.sql_query(strsql)) {
      res.send({ status: true });
    } else {
      res.send({ status: false });
    }
  } else {
    res.send({ status: false });
  }
};

exports.TableShow = async function(req, res) {
  const strsql = `SELECT * FROM [DBcomputer].[dbo].[datadicH] 
  WHERE dataid = '${req.body.id}'
  `;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.AddDataDic = async function(req, res) {
  const strsqlchk = `SELECT * FROM [DBcomputer].[dbo].[datadicH] WHERE tablename = '${req.body.table}'`;
  const resultchk = await resql.sql_query(strsqlchk);
  if (resultchk.recordset.length !== 1) {
    let current = moment().format("YYYY-MM-DD HH:mm:ss");
    const doc = `SELECT max(docno) as docnomax FROM [DBcomputer].[dbo].[datadicH]`;
    const resdoc = await resql.sql_query(doc);
    const count = `SELECT COUNT(*) as countmax FROM [DBcomputer].[dbo].[datadicH]`;
    const rescount = await resql.sql_query(count);
    let counttop;
    if (rescount.recordset[0].countmax === 0) {
      counttop = 1;
    } else {
      counttop = rescount.recordset[0].countmax + 1;
    }
    let d;
    let sv = "DIC";
    if (resdoc.recordset[0].docnomax === null) {
      d = "001";
    } else {
      d = parseInt(resdoc.recordset[0].docnomax.substr(3, 3)) + 1;
    }
    sv = sv + numeral(d).format("000");
    const strsql = `INSERT INTO [DBcomputer].[dbo].[datadicH]
            (docno , dataid , num , tablename , tablenameth , descriptiontable , preby , predate)
            VALUES ('${sv}','${req.body.dataid}' , '${counttop}' , '${req.body.table}' , '${req.body.tableth}',
            '${req.body.descriptiontb}' , '${req.body.preby}' , '${current}')
            `;

    let sqlArray = [];
    req.body.data.map((item, index) => {
      strsql2 = `INSERT INTO [DBcomputer].[dbo].[datadicD]
                  (docno , no , attribute , description , datatype , length , [key] ,
                   reference , original_position , preby , predate)
                   VALUES ('${sv}' ,
                   '${index}' ,
                   '${item.COLUMN_NAME}' ,
                   '${item.description !== "" ? item.description : ""}' ,
                   '${item.DATA_TYPE}' ,
                   '${
                     item.DATA_TYPE === "numeric"
                       ? item.NUMERIC_PRECISION + "," + item.NUMERIC_SCALE
                       : item.CHARACTER_MAXIMUM_LENGTH === null
                       ? "-"
                       : item.CHARACTER_MAXIMUM_LENGTH
                   }' ,
                   '${item.key !== "" ? item.key : ""}' ,
                   '${item.ref !== "" ? item.ref : ""}' ,
                   '${item.ORDINAL_POSITION}' ,
                   '${req.body.preby}' ,
                   '${current}')
                    `;
      return sqlArray.push({ sql: strsql2 });
    });

    const transaction = await new sql.Transaction(resql.conn);
    transaction.begin(async err => {
      try {
        const request = new sql.Request(transaction);
        const result1 = await request.query(strsql);
        for (let i = 0; i < sqlArray.length; i++) {
          await request.query(sqlArray[i].sql);
        }
        res.send({ status: true });
        transaction.commit(tErr => tErr && next("transaction commit error"));
      } catch (err) {
        console.log(err);
        res.send({ status: false });
        transaction.rollback(
          tErr => tErr && next("transaction rollback error")
        );
      }
    });
  } else {
    res.send({ status: false });
  }
};

exports.LoadDataEdit = async (req, res) => {
  const strsql = `SELECT H.docno , H.tablename ,
  H.tablenameth , H.descriptiontable ,
  H.preby , H.predate ,
  D.attribute , D.[description] ,
  D.datatype , D.[length] ,
  D.[key] , D.reference ,
  D.original_position
  FROM [DBcomputer].[dbo].[datadicH] H
  INNER JOIN [DBcomputer].[dbo].[datadicD] D
  ON H.docno = D.docno
  WHERE H.docno = '${req.body.id}'`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditDataDic = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  const strsql = `UPDATE [DBcomputer].[dbo].[datadicH]
  SET 
  [DBcomputer].[dbo].[datadicH].tablenameth = '${req.body.tableth}' , 
  [DBcomputer].[dbo].[datadicH].descriptiontable = '${req.body.descriptiontb}' , 
  [DBcomputer].[dbo].[datadicH].lastby = '${req.body.lastby}' , 
  [DBcomputer].[dbo].[datadicH].lastdate = '${current}' 
  WHERE [DBcomputer].[dbo].[datadicH].docno = '${req.body.id}' `;
  const strsql1 = `DELETE FROM [DBcomputer].[dbo].[datadicD] 
  WHERE [DBcomputer].[dbo].[datadicD].docno = '${req.body.id}'`;
  let sqlArray = [];
  req.body.data.map((item, index) => {
    strsql2 = `INSERT INTO [DBcomputer].[dbo].[datadicD]
              (docno , no , attribute , description , datatype , length , [key] ,
              reference , original_position , preby , predate , lastby , lastdate)
              VALUES ('${req.body.id}' ,
                   '${index}' ,
                   '${item.attribute}' ,
                   '${item.description !== "" ? item.description : ""}' ,
                   '${item.datatype}' ,
                   '${item.length === null ? "-" : item.length}' ,
                   '${item.key !== "" ? item.key : ""}' ,
                   '${item.reference !== "" ? item.reference : ""}' ,
                   '${item.original_position}' ,
                   '${item.preby}' ,
                   '${moment(item.predate)
                     .utc()
                     .format("YYYY-MM-DD HH:mm:ss")}',
                    '${req.body.lastby}' ,
                    '${current}')
                    `;
    return sqlArray.push({ sql: strsql2 });
  });
  const transaction = await new sql.Transaction(resql.conn);
  transaction.begin(async err => {
    try {
      const request = new sql.Request(transaction);
      const result1 = await request.query(strsql);
      const result2 = await request.query(strsql1);
      for (let i = 0; i < sqlArray.length; i++) {
        await request.query(sqlArray[i].sql);
      }
      res.send({ status: true });
      transaction.commit(tErr => tErr && next("transaction commit error"));
    } catch (err) {
      console.log(err);
      res.send({ status: false });
      transaction.rollback(tErr => tErr && next("transaction rollback error"));
    }
  });
};

exports.DeleteDataDic = async (req, res) => {
  const strsql = `DELETE FROM [DBcomputer].[dbo].[datadicH] 
  WHERE [DBcomputer].[dbo].[datadicH].docno = '${req.body.id}'`;
  const strsql1 = `DELETE FROM [DBcomputer].[dbo].[datadicD]
  WHERE [DBcomputer].[dbo].[datadicD].docno = '${req.body.id}'`;
  const transaction = await new sql.Transaction(resql.conn);
  transaction.begin(async err => {
    try {
      const request = new sql.Request(transaction);
      const result = await request.query(strsql);
      const result1 = await request.query(strsql1);
      res.send({ status: true });
      transaction.commit(tErr => tErr && next("transaction commit error"));
    } catch (err) {
      console.log(err);
      res.send({ status: false });
      transaction.rollback(tErr => tErr && next("transaction rollback error"));
    }
  });
};

exports.LoadBasename = async (req, res) => {
  const strsql = `SELECT * FROM [DBcomputer].[dbo].[basename]`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.LoadDataDictionaryShow = async (req, res) => {
  const strsql = `SELECT B.dataID , B.dbname , B.ipserver ,
  H.docno , H.num , H.tablename , H.tablenameth , H.descriptiontable ,
  D.attribute , D.[description] , D.datatype , D.[length] , D.[key] , D.reference ,
  D.original_position
  FROM [DBcomputer].[dbo].[basename] B
  INNER JOIN [DBcomputer].[dbo].[datadicH] H
  ON B.dataid = H.dataid
  INNER JOIN [DBcomputer].[dbo].[datadicD] D
  ON H.docno = D.docno
  WHERE B.dbname = '${req.body.database}'`;
  const result = await resql.sql_query(strsql);
  const strsql1 = `SELECT * FROM [DBcomputer].[dbo].[datadicH] 
  WHERE dataid = '${req.body.id}'`;
  const result1 = await resql.sql_query(strsql1);
  res.send({ status: true, data: result.recordset, dataH: result1.recordset });
};
