const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.DataApp = async function(req, res) {
  let strsql = `SELECT * FROM [DBproit].[dbo].[app] ORDER BY appID ASC`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.GenAppID = async (req, res) => {
  let strsql = `SELECT MAX(appID) as maxappid FROM [DBproit].[dbo].[app]`;
  let result = await resql.sql_query(strsql);
  let g;
  let b = numeral(
    parseInt(result.recordset[0].maxappid.substr(3, 3)) + 1
  ).format("000");
  if (result.recordset[0].maxappid !== null) {
    g = result.recordset[0].maxappid.substr(0, 3) + b;
  } else {
    g = "APP001";
  }
  res.send({ status: true, data: g });
};

exports.AddApp = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let strsql = `INSERT INTO [DBproit].[dbo].[app] 
  (appID , appName , appNameEng , status , pfs_id , developer , reMark ,
   preby , predate , telDeveloper , depUse , telDepUse , 
   programLanguage , scop) VALUES ('${req.body.appID}' , 
   '${req.body.appName}' , '${req.body.appNameEng}' , '0' , 
   '${req.body.pfs_id}' , '${req.body.developer}' ,
   '${req.body.reMark}' , '${req.body.preby}' , '${current}' , 
   '${req.body.telDeveloper}' , '${req.body.depUse}' , '${req.body.telDepUse}' , 
   '${req.body.programLanguage}' , '${req.body.scop}')`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};

exports.DataAppEdit = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[app] WHERE appID = '${req.body.appID}'`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditApp = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let sqlArray = [];
  let strsqldelete = `DELETE FROM [DBproit].[dbo].[app] WHERE appID = '${req.body.appID}'`;
  sqlArray.push({ sql: strsqldelete });

  let strsql = `INSERT INTO [DBproit].[dbo].[app] 
  (appID , appName , appNameEng , status , pfs_id , developer , reMark ,
   preby , predate , lastby , lastdate ,
   telDeveloper , depUse , telDepUse , 
   programLanguage , scop) VALUES ('${req.body.appID}' , 
   '${req.body.appName}' , '${req.body.appNameEng}' , '0' , 
   '${req.body.pfs_id}' , '${req.body.developer}' ,
   '${req.body.reMark}' , '${req.body.preby}' , '${req.body.predate}' ,
   '${req.body.lastby}' , '${current}' ,
   '${req.body.telDeveloper}' , '${req.body.depUse}' , '${req.body.telDepUse}' , 
   '${req.body.programLanguage}' , '${req.body.scop}')`;
  sqlArray.push({ sql: strsql });

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};

exports.DeleteApp = async (req, res) => {
  let strsql = `DELETE FROM [DBproit].[dbo].[app] WHERE appID = '${req.body.appID}'`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};
