const resql = require("../sql");
const crypto = require("crypto");
const moment = require("moment");
const randomString = require("randomstring");

exports.Register = async function(req, res) {
  let rDS = randomString.generate({ length: 30, charset: "alphabetic" });
  let pass = crypto
    .createHmac("sha512", rDS)
    .update(req.body.password)
    .digest("hex");
  const current = moment().format("YYYY-MM-DD HH:mm:ss");
  const strsql = `INSERT INTO [DBproit].[dbo].[users]
        (userID , salt , pass , userLevel , isexist , sta_usr , 
         sname , post , depcode , depname , preby , predate)
        VALUES
        ('${req.body.userID}' , '${rDS}' , '${pass}' , 'A' ,
         '0' , '0' , 'ADMIN' , 'ADMIN' , 'ADMIN' , 
         'ADMIN' , 'ADMIN' , '${current}' ) `;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};
