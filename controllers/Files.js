const resql = require("../sql");
const moment = require("moment");

exports.files = async function(req, res) {
  let strSql = `SELECT * FROM [DBproit].[dbo].[files] ORDER BY fileID`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.checkFile = async (req, res) => {
  let strSql = `SELECT * FROM [DBproit].[dbo].[files] WHERE fileID ='${req.params.fileid}'`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.addFile = async function(req, res) {
  const { fileID, fileName, detail, addby } = req.body;
  let strSql = `INSERT INTO [DBproit].[dbo].[files] (fileID,filename,preby,predate)
    VALUES ('${fileID}','${fileName}','${addby.toUpperCase()}','${moment().format(
    "YYYY-MM-DD HH:mm:ss"
  )}')`;
  let sqlArray = [{ sql: strSql }];
  detail.forEach(value => {
    strSql = `INSERT INTO [DBproit].[dbo].[usertrn] (userID,fileID,act_open,act_view,act_add,act_edit,act_delete,act_copy,act_print,act_other,open_cnt)
            VALUES (
                '${value.userID}',
                '${fileID}',
                 ${value.act_open ? 1 : 0},
                 ${value.act_view ? 1 : 0},
                 ${value.act_add ? 1 : 0},
                 ${value.act_edit ? 1 : 0},
                 ${value.act_delete ? 1 : 0},
                 ${value.act_copy ? 1 : 0},
                 ${value.act_print ? 1 : 0},
                 ${value.act_other ? 1 : 0},
                 0
            )`;
    return sqlArray.push({ sql: strSql });
  });
  let result = await resql.sqlTransaction(sqlArray);
  res.send(result);
};

exports.editFile = async function(req, res) {
  const { fileID, fileName, detail, addby } = req.body;
  let strSql = `UPDATE [DBproit].[dbo].[files] SET
             filename ='${fileName}'
             ,lastby='${addby.toUpperCase()}'
             ,lastdate='${moment().format("YYYY-MM-DD HH:mm:ss")}'
             WHERE fileID='${fileID}'`;

  let sqlArray = [{ sql: strSql }];

  strSql = `DELETE FROM [DBproit].[dbo].[usertrn] WHERE fileID ='${fileID}'`;

  sqlArray.push({ sql: strSql });

  detail.forEach(value => {
    strSql = `INSERT INTO [DBproit].[dbo].[usertrn] (userID,fileID,act_open,act_view,act_add,act_edit,act_delete,act_copy,act_print,act_other,open_cnt)
          VALUES (
              '${value.userID}',
              '${fileID}',
               ${value.act_open ? 1 : 0},
               ${value.act_view ? 1 : 0},
               ${value.act_add ? 1 : 0},
               ${value.act_edit ? 1 : 0},
               ${value.act_delete ? 1 : 0},
               ${value.act_copy ? 1 : 0},
               ${value.act_print ? 1 : 0},
               ${value.act_other ? 1 : 0},
               ${value.open_cnt}
          )`;

    return sqlArray.push({ sql: strSql });
  });
  let result = await resql.sqlTransaction(sqlArray);
  res.send(result);
};

exports.deleteFile = async function(req, res) {
  let sqlArray = [
    {
      sql: `DELETE FROM [DBproit].[dbo].[files] WHERE fileID ='${req.params.fileid}'`
    },
    {
      sql: `DELETE FROM [DBproit].[dbo].[usertrn] WHERE fileID ='${req.params.fileid}'`
    }
  ];
  await resql.executeTransaction(sqlArray, err => {
    if (!err) {
      res.send({ status: true });
    } else {
      res.send({ status: false });
    }
  });
  let result = await resql.sqlTransaction(sqlArray);
  res.send(result);
};
