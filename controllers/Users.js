const resql = require("../sql");
const moment = require("moment");
const { genRandomString, sha512 } = require("../module");

exports.users = async (req, res) => {
  let strSql = `SELECT userID,userLevel,isexist,sta_usr,sname,post,depcode,depname,
  lastlogin,ip_login,preby,predate,lastby,lastdate 
  FROM [DBproit].[dbo].[users] ORDER BY userID`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.checkUsers = async (req, res) => {
  let strSql = `SELECT userID FROM [DBproit].[dbo].[users] 
  WHERE userID ='${req.params.id}'`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.permission = async (req, res) => {
  let strSql = `SELECT u.userID , u.userLevel , u.isexist ,
  u.sta_usr , u.sname , u.post , u.depcode , u.depname ,
  ut.fileID , f.filename , ut.open_cnt , ut.last_date ,
  ut.act_open , ut.act_view , ut.act_add , ut.act_edit ,
  ut.act_delete , ut.act_copy , ut.act_print , ut.act_other 
  FROM [DBproit].[dbo].[users] u 
  INNER JOIN [DBproit].[dbo].[usertrn] ut
  ON u.userID = ut.userID
  INNER JOIN [DBproit].[dbo].[files] f 
  ON ut.fileID = f.fileID 
  WHERE u.userID='${req.params.id}' ORDER BY u.userID`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.permissionFiles = async (req, res) => {
  let strSql = `SELECT u.userID , u.userLevel , u.isexist ,
  u.sta_usr , u.sname , u.post , u.depcode , u.depname ,
  ut.fileID , f.filename , ut.open_cnt , ut.last_date ,
  ut.act_open , ut.act_view , ut.act_add , ut.act_edit ,
  ut.act_delete , ut.act_copy , ut.act_print , ut.act_other 
  FROM [DBproit].[dbo].[users] u 
  INNER JOIN [DBproit].[dbo].[usertrn] ut
  ON u.userID = ut.userID
  INNER JOIN [DBproit].[dbo].[files] f 
  ON ut.fileID = f.fileID 
  WHERE ut.fileID='${req.params.id}' ORDER BY ut.fileID`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.addUsers = async (req, res) => {
  const {
    Name,
    UserID,
    Passwd,
    DepName,
    Post,
    Level,
    detail,
    addby
  } = req.body;
  let salt = genRandomString(30);
  let strSql = `INSERT INTO [DBproit].[dbo].[users] (userID,salt,pass,userLevel,isexist,sta_usr,sname,post,depname,preby,predate)
              VALUES (
                '${UserID.toUpperCase()}',
                '${salt}',
                '${sha512(Passwd, salt).passwordHash}',
                '${Level}',
                0,0,
                '${Name}',
                '${Post}',
                '${DepName}',
                '${addby.toUpperCase()}',
                '${moment().format("YYYY-MM-DD HH:mm:ss")}')`;

  let sqlArray = [{ sql: strSql }];
  detail.forEach(value => {
    strSql = `INSERT INTO [DBproit].[dbo].[usertrn] (userID,fileID,act_open,act_view,act_add,act_edit,act_delete,act_copy,act_print,act_other,open_cnt)
              VALUES (
                  '${UserID.toUpperCase()}',
                  '${value.fileID}',
                   ${value.act_open ? 1 : 0},
                   ${value.act_view ? 1 : 0},
                   ${value.act_add ? 1 : 0},
                   ${value.act_edit ? 1 : 0},
                   ${value.act_delete ? 1 : 0},
                   ${value.act_copy ? 1 : 0},
                   ${value.act_print ? 1 : 0},
                   ${value.act_other ? 1 : 0},
                   0
              )`;
    return sqlArray.push({ sql: strSql });
  });

  let result = await resql.sqlTransaction(sqlArray);
  res.send(result);
};

exports.editUsers = async (req, res) => {
  const { Name, UserID, DepName, Post, Level, detail, addby } = req.body;
  let strSql = `UPDATE [DBproit].[dbo].[users] SET userLevel='${Level}'
                  ,sname='${Name}'
                  ,post='${Post}'
                  ,depname='${DepName}'
                  ,lastby='${addby.toUpperCase()}'
                  ,lastdate='${moment().format("YYYY-MM-DD HH:mm:ss")}'
                  WHERE userID ='${UserID}'`;

  let sqlArray = [{ sql: strSql }];

  strSql = `DELETE FROM [DBproit].[dbo].[usertrn] WHERE userID ='${UserID}'`;

  sqlArray.push({ sql: strSql });
  detail.forEach(value => {
    strSql = `INSERT INTO [DBproit].[dbo].[usertrn] (userID,fileID,act_open,act_view,act_add,act_edit,act_delete,act_copy,act_print,act_other,open_cnt)
            VALUES (
                '${UserID.toUpperCase()}',
                '${value.fileID}',
                 ${value.act_open ? 1 : 0},
                 ${value.act_view ? 1 : 0},
                 ${value.act_add ? 1 : 0},
                 ${value.act_edit ? 1 : 0},
                 ${value.act_delete ? 1 : 0},
                 ${value.act_copy ? 1 : 0},
                 ${value.act_print ? 1 : 0},
                 ${value.act_other ? 1 : 0},
                 0
            )`;
    return sqlArray.push({ sql: strSql });
  });

  let result = await resql.sqlTransaction(sqlArray);
  res.send(result);
};

exports.deleteUsers = async function(req, res) {
  let sqlArray = [
    {
      sql: `DELETE FROM [DBproit].[dbo].[users] WHERE userID ='${req.params.id}'`
    },
    {
      sql: `DELETE FROM [DBproit].[dbo].[usertrn] WHERE userID ='${req.params.id}'`
    }
  ];
  let result = await resql.sqlTransaction(sqlArray);
  res.send(result);
};
