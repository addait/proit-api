const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.DataSoftware = async function(req, res) {
  const strsql = `SELECT a.barcode , a.assetID , a.typeID ,
  a.brandID , a.name , a.cdkey , a.volumelicense , a.price ,
  a.unit , a.warranty , a.startdate , a.locacode , a.locadesc ,
  a.pfs_id1 , a.name1 , a.pfs_id2 , a.name2 , a.preby , a.predate ,
  a.lastby , a.lastdate , b.brandName
  FROM [DBproit].[dbo].[asset] a
  INNER JOIN [DBproit].[dbo].[brand] b
  ON a.brandID = b.brandID`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataAsset = async (req, res) => {
  const strsql = `WITH asset AS (SELECT a.DEPRE_GL , b.DEPRE_GL as DEPRE_GL1 , 
    a.ASS_GL , b.ASS_GL as ASS_GL1 , a.[GROUP] , b.[DESC] as b_desc ,
    a.CODE , c.[DESC] as c_desc , a.ASCODE , a.[DESC] as a_desc ,
    a.UNIT , a.QTY , a.REFNO , a.UNITCOST , a.HOLD , a.STARTDATE ,
    a.ENDDATE , a.[YEAR] , a.METHOD , a.[PERCENT] , a.SALVAGE ,
    ROW_NUMBER() OVER (ORDER BY a.ascode DESC) AS 'RowNumber' ,
    a.locacode , a.locadesc , a.pfs_date1 , pfs_doc1 , a.pfs_id1 , a.name1 ,
    a.pfs_date2 , pfs_doc2 , a.pfs_id2 , a.name2 ,
    a.pfs_date3 , pfs_doc3 , a.pfs_id3 , a.name3 ,
    CASE a.hold WHEN 'S' THEN 'ไม่มีตัวตน' ELSE CASE a.DEP_AMTN 
    WHEN '2' THEN 'ไม่มีตัวตน' WHEN '1' THEN 'คงอยู่' ELSE 'รอตรวจสอบ' END END AS sta_adt ,
    a.ME_CODE
    FROM [DBfixasst_gp].[dbo].[asmstr] a
    INNER JOIN [DBfixasst_gp].[dbo].[asgroup] b
    ON a.[GROUP] = b.[GROUP]
    INNER JOIN [DBfixasst_gp].[dbo].[ascost] c
    ON a.CODE = c.CODE WHERE a.[GROUP] = 'FS')
    SELECT * FROM asset WHERE sta_adt = 'คงอยู่'`;
  const result = await resql.sql_query14(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataType = async (req, res) => {
  const strsql = `SELECT * FROM [DBproit].[dbo].[catagory]`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataBrand = async (req, res) => {
  const strsql = `SELECT * FROM [DBproit].[dbo].[brand]`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataSeller = async (req, res) => {
  const strsql = `SELECT * FROM [DBproit].[dbo].[supplier]`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataLocation = async (req, res) => {
  const strsql = `SELECT * FROM [DBfixasst_gp].[dbo].[asloca]`;
  const result = await resql.sql_query14(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.GenIDasset = async (req, res) => {
  const current = moment().format("DD/MM/YYYY");
  // console.log(current.substr(3,2))
  // console.log(current.substr(8,2))
  let search = req.body.id + current.substr(8, 2) + current.substr(3, 2);
  const strsql = `SELECT MAX(barcode) as maxbarcode 
  FROM [DBproit].[dbo].[asset] WHERE barcode LIKE '${search}%'`;
  const result = await resql.sql_query(strsql);
  let num;
  if (result.recordset[0].maxbarcode === null) {
    num = "0001";
  } else {
    num = parseInt(result.recordset[0].maxbarcode.substr(7, 4)) + 1;
  }
  search = search + numeral(num).format("0000");
  res.send({ status: true, data: search });
};

exports.AddAsset = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  const strsql = `INSERT INTO [DBproit].[dbo].[asset]
  (barcode , assetID , typeID , brandID , name , partnumber , 
   serial , isOS , cdkey , volumelicense , price , unit ,
   warranty , invno , supplierID , depcode , status , hold ,
   startdate , startend , locacode , locadesc , pfs_date1 ,
   pfs_id1 , name1 , pfs_date2 , pfs_id2 , name2 , preby ,
   predate , conf) 
   VALUES ('${req.body.barcode}' , '${req.body.ascode}' , 
   '${req.body.idtype}' , '${req.body.idbrand}' , '${req.body.a_desc}' , 
   '${req.body.partnumber}' ,'${req.body.serial}' , 
   '${req.body.checkOS ? 1 : 0}' , '${req.body.cdkey}' , 
   '${req.body.volumelicense}' , '${req.body.price}' ,
   '${req.body.unit}' , '${req.body.warranty}' , 
   '${req.body.refno}' , '${req.body.idseller}' , '${req.body.deptcode}' ,
   '0' , '${req.body.hold}' , '${req.body.startwarranty}' , 
   '${req.body.endwarranty}' , '${req.body.idlocation}' , 
   '${req.body.namelocation}' , '${req.body.pfs_date1}' , '${req.body.pfs_id1}',
   '${req.body.name1}' , '${req.body.pfs_date2}' , '${req.body.pfs_id2}' ,
   '${req.body.name2}' , '${req.body.preby}' , '${current}' , '0')`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};

exports.DataSoftwareEdit = async (req, res) => {
  const strsql = `SELECT a.barcode , a.assetID , a.typeID , c.typename ,
  a.brandID , b.brandName , a.name , a.partnumber ,
  a.serial , a.isOS , a.cdkey , a.volumelicense ,
  a.price , a.unit , a.warranty , a.invno , 
  a.supplierID , d.supplierName ,
  a.depcode , e.[desc] , a.status , a.hold , a.startdate , a.startend ,
  a.locacode , a.locadesc , a.pfs_date1 , a.pfs_id1 , a.name1 ,
  a.pfs_date2 , a.pfs_id2 , a.name2 , a.preby , a.predate 
  FROM [DBproit].[dbo].[asset] a
  INNER JOIN [DBproit].[dbo].[brand] b
  ON a.brandID = b.brandID
  INNER JOIN [DBproit].[dbo].[catagory] c
  ON a.typeID = c.typeID
  INNER JOIN [DBproit].[dbo].[supplier] d
  ON a.supplierID = d.supplierID
  INNER JOIN [DBproit].[dbo].[gldept] e
  ON a.depcode = e.depcode WHERE barcode = '${req.body.id}'`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditAsset = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let sqlArray = [];
  const strsqldelete = `DELETE FROM [DBproit].[dbo].[asset] WHERE barcode = '${req.body.barcode}'`;
  sqlArray.push({ sql: strsqldelete });

  const strsql = `INSERT INTO [DBproit].[dbo].[asset]
  (barcode , assetID , typeID , brandID , name , partnumber , 
   serial , isOS , cdkey , volumelicense , price , unit ,
   warranty , invno , supplierID , depcode , status , hold ,
   startdate , startend , locacode , locadesc , pfs_date1 ,
   pfs_id1 , name1 , pfs_date2 , pfs_id2 , name2 , preby ,
   predate , lastby , lastdate , conf) 
   VALUES ('${req.body.barcode}' , '${req.body.ascode}' , 
   '${req.body.idtype}' , '${req.body.idbrand}' , '${req.body.a_desc}' , 
   '${req.body.partnumber}' ,'${req.body.serial}' , 
   '${req.body.checkOS ? 1 : 0}' , '${req.body.cdkey}' , 
   '${req.body.volumelicense}' , '${req.body.price}' ,
   '${req.body.unit}' , '${req.body.warranty}' , 
   '${req.body.refno}' , '${req.body.idseller}' , '${req.body.deptcode}' ,
   '0' , '${req.body.hold}' , '${req.body.startwarranty}' , 
   '${req.body.endwarranty}' , '${req.body.idlocation}' , 
   '${req.body.namelocation}' , '${req.body.pfs_date1}' , '${req.body.pfs_id1}',
   '${req.body.name1}' , '${req.body.pfs_date2}' , '${req.body.pfs_id2}' ,
   '${req.body.name2}' , '${req.body.preby}' , '${req.body.predate}' , 
   '${req.body.lastby}' , '${current}' , '0')`;

  sqlArray.push({ sql: strsql });

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};

exports.DeleteAsset = async (req, res) => {
  let strsql = `DELETE FROM [DBproit].[dbo].[asset] WHERE barcode = '${req.body.id}'`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};
