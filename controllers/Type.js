const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.DataType = async function(req, res) {
  let strsql = `WITH CS AS (
    SELECT typeID , COUNT(typeID) as quantity , SUM(price) as priceall FROM [DBproit].[dbo].[asset]
    GROUP BY typeID )
    SELECT b.typeID , b.typename , a.quantity , 
    a.priceall , b.preby , b.predate ,
    b.lastby , b.lastdate
    FROM CS a 
    RIGHT JOIN [DBproit].[dbo].[catagory] b
    ON a.typeID = b.typeID`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.AddDataType = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let strsql = `INSERT INTO [DBproit].[dbo].[catagory]
    (typeID , typename , preby , predate) VALUES 
    ('${req.body.typeID}' , '${req.body.typeName}' ,
     '${req.body.preby}' , '${current}')`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};

exports.DataTypeEdit = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[catagory] 
    WHERE typeID = '${req.body.typeID}'`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditDataType = async (req, res) => {
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let sqlArray = [];
  let strsqldelete = `DELETE FROM [DBproit].[dbo].[catagory] 
  WHERE typeID = '${req.body.typeID}'`;
  sqlArray.push({ sql: strsqldelete });

  let strsql = `INSERT INTO [DBproit].[dbo].[catagory]
      (typeID , typename , preby , predate , lastby , lastdate) VALUES 
      ('${req.body.typeID}' , '${req.body.typeName}' ,
       '${req.body.preby}' , '${req.body.predate}' , 
       '${req.body.lastby}' , '${current}')`;
  sqlArray.push({ sql: strsql });

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};

exports.DeleteDataType = async (req, res) => {
  let strsql = `DELETE FROM [DBproit].[dbo].[catagory] WHERE typeID = '${req.body.typeID}'`;
  if (await resql.sql_query(strsql)) {
    res.send({ status: true });
  } else {
    res.send({ status: false });
  }
};
