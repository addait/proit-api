const resql = require("../sql");
const sql = require("mssql");
const crypto = require("crypto");
const moment = require("moment");
const ip = require("ip");
const randomString = require("randomstring");

exports.LogIn = async function(req, res) {
  const current = moment().format("YYYY-MM-DD HH:mm:ss");
  const rDS = randomString.generate({ length: 30, charset: "alphabetic" });
  const strsql = `SELECT * FROM [DBproit].[dbo].[users] WHERE userID = '${req.body.userID}'`;
  const result = await resql.sql_query(strsql);
  if (result.recordset.length !== 0) {
    const pass = crypto
      .createHmac("sha512", result.recordset[0].salt)
      .update(req.body.password)
      .digest("hex");
    const strsql1 = `SELECT * FROM [DBproit].[dbo].[users] WHERE userID = '${req.body.userID}' and pass = '${pass}'`;
    const result1 = await resql.sql_query(strsql1);
    if (result1.recordset.length !== 0) {
      const strsql2 = `UPDATE [DBproit].[dbo].[users]
      SET
      lastlogin = '${current}' , ip_login = '${ip.address()}' , token = '${rDS}'
      WHERE userID = '${req.body.userID}' `;
      if (await resql.sql_query(strsql2)) {
        res.send({ status: true, token: rDS, user: result1.recordset });
      }
    } else {
      res.send({ status: false });
    }
  } else {
    res.send({ status: false });
  }
};

exports.LogIn14 = async function(req, res) {
  const strsql = `SELECT * FROM [DBcomputer].[dbo].[usermst] WHERE user_id = '${req.body.userID}' and pass = '${req.body.password}'`;
  const result = await resql.sql_query(strsql);
  if (result.recordset.length !== 0) {
    res.send({ status: true, user: result.recordset });
  } else {
    res.send({ status: false });
  }
};
