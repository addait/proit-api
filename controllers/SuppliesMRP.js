const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.DataSuppliesMRP = async (req, res) => {
  let strsql = `SELECT a.typeID , c.typeName , a.prodcode , a.productID , b.[group] ,
  a.barcode , a.name , b.name as namemrp , a.unit , a.stdprice , a.lastprice ,
  a.wh , a.vend_code , b.glcode , b.glcode2 , a.qty ,
  a.resrv , a.minqty , a.maxqty , a.pr_qty , a.limit ,
  a.location , a.picname , a.web , a.status , a.class , 
  a.descrip , a.preby , a.predate , a.lastby , a.lastdate
  FROM [DBproit].[dbo].[item] a 
  INNER JOIN [DBproit].[dbo].[sparepart] b
  ON a.prodcode = b.prodcode
  INNER JOIN [DBproit].[dbo].[itemtype] c 
  ON a.typeID = c.typeID`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataSparepartMRP = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[sparepart]`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};
