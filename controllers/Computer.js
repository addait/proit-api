const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.UserHRM = async function(req, res) {
  let strsql = `SELECT a.pfs_id , a.title , a.name , a.last_name , a.division , b.[desc]
  FROM [DBhrm].[dbo].[prmst] a 
  INNER JOIN [DBhrm].[dbo].[gldept] b
  ON a.division = b.depcode`;
  let result = await resql.sql_query14(strsql);
  let strsql1 = `SELECT TOP 100 
  a.pfs_id , a.title , a.name , a.last_name , a.division , b.[desc]
  FROM [DBhrm].[dbo].[prmst] a
  INNER JOIN [DBhrm].[dbo].[gldept] b
  ON a.division = b.depcode
  `;
  let result1 = await resql.sql_query14(strsql1);

  res.send({
    status: true,
    data: result.recordset,
    datatop100: result1.recordset
  });
};

exports.DeptHRM = async (req, res) => {
  let strsql = `SELECT depcode , [desc] , [group] , depcodeo FROM [DBhrm].[dbo].[gldept] 
  WHERE [desc] NOT LIKE '%ยกเลิก%' AND 
  [desc] NOT LIKE '%งดใช้%' AND
  [desc] NOT LIKE '%ไม่ใช้%' AND
  [desc] NOT LIKE '%ย้ายไป%'`;
  let result = await resql.sql_query14(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.DataComputer = async function(req, res) {
  let strSql = `SELECT comID , startDate , pfs_id , nameuse , 
  depcode , [desc] , Tel , comname , ipaddress ,
  preby , predate , lastby , lastdate
  FROM [DBproit].[dbo].[computer] ORDER BY comID desc`;
  let result = await resql.sql_query(strSql);
  res.send({ status: true, data: result.recordset });
};

exports.OS = async (req, res) => {
  let strsql = `SELECT a.barcode , b.brandName , a.partnumber , a.name , a.cdkey ,
  a.volumelicense , count(c.barcode) as countb
  FROM [DBproit].[dbo].[asset] a 
  INNER JOIN [DBproit].[dbo].[brand] b 
  ON a.brandID = b.brandID
  INNER JOIN [DBproit].[dbo].[computer] c 
  ON a.barcode = c.barcode
  GROUP BY a.barcode , b.brandName , a.partnumber , a.name , a.cdkey , a.volumelicense`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.Software = async (req, res) => {
  let strsql = `SELECT a.barcode , b.brandName , a.partnumber , a.name , a.cdkey ,
  a.price , a.volumelicense , COUNT(ct.barcode) as useqty
  FROM [DBcomputer].[dbo].[asset] a
  INNER JOIN [DBproit].[dbo].[brand] b
  ON a.brandID = b.brandID
  INNER JOIN [DBproit].[dbo].[computertrn] ct
  ON a.barcode = ct.barcode
  WHERE a.isOS = '0'
  GROUP BY a.barcode , b.brandName , a.partnumber , a.name , a.cdkey ,
  a.price , a.volumelicense;`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.SoftwareAdda = async (req, res) => {
  let strsql = `SELECT * FROM [DBproit].[dbo].[app] ORDER BY appID asc`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.AddMain = async (req, res) => {
  // console.log(req.body.date)
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let v;
  if (req.body.value === "PC") {
    v = "COM";
  } else if (req.body.value === "THIN") {
    v = "TIN";
  } else if (req.body.value === "NOTEBOOK") {
    v = "NTE";
  } else if (req.body.value === "SERVER") {
    v = "SRV";
  } else if (req.body.value === "MACBOOK") {
    v = "MAC";
  } else if (req.body.value === "I-MAC") {
    v = "IMC";
  }
  const strsqlmax = `SELECT MAX(comID) as mx FROM [DBproit].[dbo].[computer]
  WHERE comID LIKE '%${v}%'`;
  const result = await resql.sql_query(strsqlmax);
  let vv;
  if (result.recordset[0].mx === null) {
    vv = "00001";
  } else {
    vv = parseInt(result.recordset[0].mx.substr(3, 5)) + 1;
  }
  vv = v + numeral(vv).format("00000");
  let b;
  if (req.body.bit === "64") {
    b = 1;
  } else {
    b = 0;
  }
  // console.log(vv)
  let data = [];
  data.push(
    ...req.body.datacpu,
    ...req.body.datamb,
    ...req.body.dataram,
    ...req.body.datahdd,
    ...req.body.datamn,
    ...req.body.datavga,
    ...req.body.datalan,
    ...req.body.datapt,
    ...req.body.dataot
  );

  const strsqls1 = `INSERT INTO [DBproit].[dbo].[computer]
  (comID , startDate , pfs_id , nameuse , depcode , [desc] , Tel ,
   comname , ipaddress , barcode , osbit , isuse , cpu , cpuSpeed ,
   ram , ramSize , hddSize , hdd , m_b , vga , lancard , monitor ,
   ups , printer , printersn , Remark , preby , predate , StatusCom) 
   VALUES ('${vv}' , '${req.body.date}' , '${req.body.pfs_id}' ,
   '${req.body.name}' , '${req.body.depcode}' , '${req.body.depname}' , '${req.body.tel}' ,
   '${req.body.pcname}' , '${req.body.ippc}' , '${req.body.barcode}' , '${b}' ,
   '1' , '${req.body.cpu}' , '${req.body.speed}' , '${req.body.ram}' , '${req.body.sizeram}' ,
   '${req.body.sizehd}' , '${req.body.harddisk}' , '${req.body.mainboard}' ,
   '${req.body.vga}' , '${req.body.lan}' , '${req.body.monitor}' , '${req.body.ups}' ,
   '${req.body.printer}' , '${req.body.printersn}' , '${req.body.note}' , '${req.body.preby}' ,
   '${current}' , '0')`;

  let sqlArray = [{ sql: strsqls1 }];

  data.map((item, index) => {
    if (item.type === "CPU") {
      item.type = 0;
    } else if (item.type === "MB") {
      item.type = 1;
    } else if (item.type === "RAM") {
      item.type = 2;
    } else if (item.type === "HDD") {
      item.type = 3;
    } else if (item.type === "MN") {
      item.type = 4;
    } else if (item.type === "VGA") {
      item.type = 5;
    } else if (item.type === "LAN") {
      item.type = 6;
    } else if (item.type === "PT") {
      item.type = 7;
    } else if (item.type === "OT") {
      item.type = 8;
    }

    strsqls2 = `INSERT INTO [DBproit].[dbo].[computer_asset]
    (type , comID , no , status , ascode , asdesc , price , startdate , remark)
    VALUES ('${item.type}' , '${vv}' , '${index}' , '${item.statusitem}' , 
    '${item.codeitem}' , '${item.nameitem}' , '${item.priceitem}' ,
    '${item.dateitem}' , '${item.noteitem}')`;
    return sqlArray.push({ sql: strsqls2 });
  });

  if (req.body.datasoftware === undefined) {
  } else {
    let dataSF = req.body.datasoftware;
    dataSF.map((item, index) => {
      strsqls3 = `INSERT INTO [DBproit].[dbo].[computertrn]
    (comID , no , type , barcode) VALUES
    ('${vv}' , '${index}' , 's' , '${item.barcode}')`;
      return sqlArray.push({ sql: strsqls3 });
    });
  }

  if (req.body.datasoftwareadda === undefined) {
  } else {
    let dataSFADDA = req.body.datasoftwareadda;
    dataSFADDA.map((item, index) => {
      strsqls4 = `INSERT INTO [DBproit].[dbo].[com_app_adda]
    (comID , appID , no , remark) VALUES 
    ('${vv}' , '${item.appID}' , '${index}' , '${item.reMark}')`;
      return sqlArray.push({ sql: strsqls4 });
    });
  }

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};

exports.LoadDataEdit = async (req, res) => {
  const strsql = `SELECT a.comID , a.startDate , a.pfs_id , a.nameuse ,
  a.depcode , a.[desc] , a.Tel , a.comname , a.ipaddress ,
  a.barcode , c.name , c.cdkey , a.osbit , a.isuse , a.cpu , a.cpuSpeed ,
  a.ram , a.ramSize , a.hddSize , a.hdd , a.m_b , a.vga ,
  a.lancard , a.monitor , a.ups , a.printer , a.printersn ,
  a.Remark , a.preby , a.predate , a.lastby , a.lastdate ,
  a.picid , a.picname , b.[type] , b.[status] , b.ascode , 
  b.asdesc , b.price , b.startdate as bstartdate , b.remark as bremark
  FROM [DBproit].[dbo].[computer] a
  INNER JOIN [DBproit].[dbo].[computer_asset] b 
  ON a.comID = b.comID
  LEFT JOIN [DBproit].[dbo].[asset] c 
  ON a.barcode = c.barcode  WHERE a.comID = '${req.body.id}'`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.LoadSoftwareEdit = async (req, res) => {
  const strsql = `SELECT a.comID , a.barcode , c.brandName , 
  b.name , b.cdkey , b.price 
  FROM [DBproit].[dbo].[computertrn] a 
  INNER JOIN [DBproit].[dbo].[asset] b
  ON a.barcode = b.barcode
  INNER JOIN [DBproit].[dbo].[brand] c
  ON b.brandID = c.brandID WHERE a.comID = '${req.body.id}'`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.LoadSoftwareAddaEdit = async (req, res) => {
  const strsql = `SELECT a.appID , b.appName , b.appNameEng , b.reMark 
  FROM [DBproit].[dbo].[com_app_adda] a
  INNER JOIN [DBproit].[dbo].[app] b
  ON a.appID = b.appID WHERE a.comID = '${req.body.id}'`;
  const result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};

exports.EditMain = async (req, res) => {
  // console.log(req.body);
  // console.log(req.body.id);
  let current = moment().format("YYYY-MM-DD HH:mm:ss");
  let b;
  if (req.body.bit === "64") {
    b = 1;
  } else {
    b = 0;
  }

  const strsqlpd = `SELECT * FROM [DBproit].[dbo].[computer] WHERE comID = '${req.body.id}'`;
  const resultpd = await resql.sql_query(strsqlpd);

  let sqlArray = [];

  const strsqld1 = `DELETE FROM [DBproit].[dbo].[computer] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld1 });

  const strsqld2 = `DELETE FROM [DBproit].[dbo].[computer_asset] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld2 });

  const strsqld3 = `DELETE FROM [DBproit].[dbo].[computertrn] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld3 });

  const strsqld4 = `DELETE FROM [DBproit].[dbo].[com_app_adda] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld4 });

  const strsqls1 = `INSERT INTO [DBproit].[dbo].[computer]
  (comID , startDate , pfs_id , nameuse , depcode , [desc] , Tel ,
   comname , ipaddress , barcode , osbit , isuse , cpu , cpuSpeed ,
   ram , ramSize , hddSize , hdd , m_b , vga , lancard , monitor ,
   ups , printer , printersn , Remark , preby , predate , lastby , lastdate , StatusCom)
   VALUES ('${req.body.id}' , '${req.body.date}' , '${req.body.pfs_id}' ,
   '${req.body.name}' , '${req.body.depcode}' , '${req.body.depname}' , '${req.body.tel}' ,
   '${req.body.pcname}' , '${req.body.ippc}' , '${req.body.barcode}' , '${b}' ,
   '1' , '${req.body.cpu}' , '${req.body.speed}' , '${req.body.ram}' , '${req.body.sizeram}' ,
   '${req.body.sizehd}' , '${req.body.harddisk}' , '${req.body.mainboard}' ,
   '${req.body.vga}' , '${req.body.lan}' , '${req.body.monitor}' , '${req.body.ups}' ,
   '${req.body.printer}' , '${req.body.printersn}' , '${req.body.note}' , 
   '${resultpd.recordset[0].preby}', '${resultpd.recordset[0].predate}' ,'${req.body.lastby}' , '${current}' , '0')`;

  sqlArray.push({ sql: strsqls1 });

  let data = [];
  data.push(
    ...req.body.datacpu,
    ...req.body.datamb,
    ...req.body.dataram,
    ...req.body.datahdd,
    ...req.body.datamn,
    ...req.body.datavga,
    ...req.body.datalan,
    ...req.body.datapt,
    ...req.body.dataot
  );

  data.map((item, index) => {
    if (item.type === "CPU") {
      item.type = 0;
    } else if (item.type === "MB") {
      item.type = 1;
    } else if (item.type === "RAM") {
      item.type = 2;
    } else if (item.type === "HDD") {
      item.type = 3;
    } else if (item.type === "MN") {
      item.type = 4;
    } else if (item.type === "VGA") {
      item.type = 5;
    } else if (item.type === "LAN") {
      item.type = 6;
    } else if (item.type === "PT") {
      item.type = 7;
    } else if (item.type === "OT") {
      item.type = 8;
    }

    strsqls2 = `INSERT INTO [DBproit].[dbo].[computer_asset]
    (type , comID , no , status , ascode , asdesc , price , startdate , remark)
    VALUES ('${item.type}' , '${req.body.id}' , '${index}' , '${item.statusitem}' ,
    '${item.codeitem}' , '${item.nameitem}' , '${item.priceitem}' ,
    '${item.dateitem}' , '${item.noteitem}')`;
    return sqlArray.push({ sql: strsqls2 });
  });

  if (req.body.datasoftware === undefined) {
  } else {
    let dataSF = req.body.datasoftware;
    dataSF.map((item, index) => {
      strsqls3 = `INSERT INTO [DBproit].[dbo].[computertrn]
      (comID , no , type , barcode) VALUES
      ('${req.body.id}' , '${index}' , 's' , '${item.barcode}')`;
      return sqlArray.push({ sql: strsqls3 });
    });
  }

  if (req.body.datasoftwareadda === undefined) {
  } else {
    let dataSFADDA = req.body.datasoftwareadda;
    dataSFADDA.map((item, index) => {
      strsqls4 = `INSERT INTO [DBproit].[dbo].[com_app_adda]
    (comID , appID , no , remark) VALUES
    ('${req.body.id}' , '${item.appID}' , '${index}' , '${item.reMark}')`;
      return sqlArray.push({ sql: strsqls4 });
    });
  }

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};

exports.DeleteData = async (req, res) => {
  let sqlArray = [];

  const strsqld1 = `DELETE FROM [DBproit].[dbo].[computer] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld1 });

  const strsqld2 = `DELETE FROM [DBproit].[dbo].[computer_asset] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld2 });

  const strsqld3 = `DELETE FROM [DBproit].[dbo].[computertrn] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld3 });

  const strsqld4 = `DELETE FROM [DBproit].[dbo].[com_app_adda] WHERE comID = '${req.body.id}'`;
  sqlArray.push({ sql: strsqld4 });

  let resultss = await resql.sqlTransaction(sqlArray);
  res.send(resultss);
};
