const resql = require("../sql");
const moment = require("moment");
const numeral = require("numeral");

exports.DataSupplier = async function(req, res) {
  let strsql = `SELECT * FROM [DBproit].[dbo].[tmp_supplier]`;
  let result = await resql.sql_query(strsql);
  res.send({ status: true, data: result.recordset });
};
