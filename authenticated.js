const resql = require("./sql");

const authenticatedMiddleware = async (req, res, next) => {
  const authToken = req.headers.authorization;
  if (authToken === "undefined" || authToken === "null" || !authToken) {
    return res.send({ status: false, data: "no token" });
  }
  // console.log(req.headers.authorization)
  const [userID, token] = authToken.split(":");
  // console.log(userID,token)
  let strsql = `SELECT token FROM [DBproit].[dbo].[users] WHERE userID = '${userID}' AND token ='${token}' `;
  let result = await resql.sql_query(strsql);

  if (result.recordset.length !== 0) {
    next();
  } else {
    res.send({ status: false, data: "you are not authenticated" });
  }
};

module.exports.authenticatedMiddleware = authenticatedMiddleware;
